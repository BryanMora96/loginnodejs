//Contentiene el esquema que de la base de datos
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
//Se crea la constante para manejo de la seguridad
const bcrypt = require('bcryptjs');

//Se crea nuevo Schema para el manejo del Login en la aplicacion
const userShema = new Schema({
    username: String,
    email: String,
    password: String
});

//Se crea para este metodo para la encriptacion de la contraseña
userShema.methods.encryptPassword = async function (password)  {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
};

//funcion para validar el password
userShema.methods.validatePassword = function(password) {
    return bcrypt.compare(password, this.password);
};

module.exports = mongoose.model('user', userShema);