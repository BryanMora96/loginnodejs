let mongoose = require('mongoose');

//contiene cada uno de los campos que seran almacenados en la base de datos
let productSchema = mongoose.Schema({
    name: {
        type: String,
        require: true,
    },
    price: Number,
    stock: Number, 
    create: {
        type: Date,
        default: Date.now
    }
});

let Product = module.exports = mongoose.model('product', productSchema);

//Cada vez que hagamos una peticion get se ejecutara esto
module.exports.get = function(callback, limit){
    Product.find(callback).limit(limit);
}