const Product = require('../models/ProductsModels');

exports.index = (req, res) => {
    Product.get((err, product) => {
        if (err) {
            res.json({
                status: 'err',
                code: 500,
                message: err
            })
        }
        res.json(product);
    });
}

exports.new = (req, res) => {
    const product = new Product()
    product.name = req.body.name
    product.price = req.body.price
    product.stock = req.body.stock
    product.save(function (err) {
        if (err) {
            res.json({
                status: 'err',
                code: 500,
                message: err
            })
        }
        res.json({
            status: 'success',
            code: 200,
            message: 'Registro Creado',
            data: product
        });
    });
}

exports.view = function(req, res) {
    Product.findById(req.params.id, function(err, product){
        if(err) {
            res.json({
                status: 'err',
                code: 500,
                message: err
            })
        }
        res.json({
            status: 'success',
            code: 200,
            message: 'Registros Encontrado',
            data: product
        })
    })
}

exports.update = (req, res) => {
    Product.findById(req.params.id, function (err, product) {
        if (err) {
            res.json({
                status: 'err',
                code: 500,
                message: err
            })
        }
        product.name = req.body.name
        product.price = req.body.price
        product.stock = req.body.stock
        product.save(function (err) {
            if (err)
                res.json({
                    status: 'err',
                    code: 500,
                    message: err
                })
            res.json({
                status: 'success',
                code: 200,
                message: 'Registro Actualizado',
                data: product
            })
        })
    })
} 

exports.delete = function(req, res) {
    Product.remove({
        _id: req.params.id
    }, function(err){
        if (err)
            res.json({
                status: 'err',
                code: 500,
                message: err
            })
        res.json({
            status: 'success',
            code: 200,
            message: 'Registro Eliminado'
        })
    })
}

/*
router.get('/delete/:id', async (req, res) => {
    const { id } = req.params;
    await Task.remove({_id: id});
    res.redirect('/');
});
*/







