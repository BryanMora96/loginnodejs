//Se utiliza para las rutas que intentamos proteger, a los usuarios que se encuentren logueados, verificar si tienen el permiso para utilziar esa ruta 
const jwt = require('jsonwebtoken');
const config = require('../config');

async function verifyToken(req, res, next) {
    const token = req.headers['x-access-token'];
    if(!token) {
        return res.status(401).send({
            auth: fasle,
            message: 'No token provided'
        });
    }
    const decoded = await jwt.verify(token, config.secret);
    req.userId = decoded.Id;

    next();
}

module.exports = verifyToken;