const express = require('express');
const router = express.Router();

//import del task.js
const Task = require('../models/task');
//cada una de estas funciones ser manejara de manera asincrona a la base de datoss 

//Sera la ruta con la que se inicia el app y mostrara los datos que se encuentran en la base de datos
router.get('/', async (req, res) => {
    const tasks = await Task.find();
    res.render('index', {
        tasks
    });
});

//Esta ruta manejara el insert a la base de datos
router.post('/add', async (req, res) => {
    const task = new Task(req.body);
    console.log(req.body);
    await task.save();
    res.redirect('/');
});

//Esta ruta permitira el cambio de estado en el campo de estado (puede utilizarse para el manejo de un evento activo o inactivo)
//Esta actividad se ralizara a traves del "id" del registro
router.get('/turn/:id', async (req, res) => {
    const { id } = req.params;
    const task = await Task.findById(id);
    task.status = !task.status;
    await task.save();
    res.redirect('/');
});

//Esta ruta consultara en la base de dato los registros asociados al "id"
router.get('/edit/:id', async (req, res) => {
    const { id } = req.params;
    const task = await Task.findById(id);
    res.render('edit', {
        task
    });
});

//Esta ruta permitira la edicion de cualquiera de los registros a traves del "id"
router.post('/edit/:id', async (req, res) => {
    const { id } = req.params;
    await Task.update({_id: id}, req.body);
    res.redirect('/');
});

//A traves de esta ruta se podra realizar el delete o eliminar cualquiera de los registros en la base de datos, usando el "id"
router.get('/delete/:id', async (req, res) => {
    const { id } = req.params;
    await Task.remove({_id: id});
    res.redirect('/');
});

module.exports = router;