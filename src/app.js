const path = require('path');
const express = require('express');
const morgan = require('morgan');
const app = express();
const mongoose = require('mongoose');

//conexion a la base de datos y control de excepcion con una promesa
//la base de datos llevara el nombre de "portal-eventos" y se creara automaticamente
mongoose.connect('mongodb://localhost/portal-eventos')
    .then(db => console.log('DB conectada'))
    .catch(err => console.log(err));

//importacion del archivo indx que contiene las rutas para realizar el CRUD
const indexRoutes = require('./routes/index');

//control del puerto para la ejecucion de NodeJs
app.set('port', process.env.PORT || 3000);
//
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended: false}));

//rutas 
app.use('/', indexRoutes);

app.use(express.json());
app.use(require('./controllers/authController'));
 
//Ejecucion del servidor a traves del puerto configurado
app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'));
});
